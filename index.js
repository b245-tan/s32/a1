const http = require('http');

let port = 4000;

let directory = [];

http.createServer(function(request, response){

	if(request.url === "/" && request.method === "GET"){
			response.writeHead(200, {'Content-Type': 'text/plain'});
			response.end("Welcome to booking system");
	} 


	else if(request.url === "/profile" && request.method === "GET"){
			response.writeHead(200, {'Content-Type': 'text/plain'});
			response.end("Welcome to your profile");
	}


	else if(request.url === "/courses" && request.method === "GET"){
			response.writeHead(200, {'Content-Type': 'application/json'});
			response.write("Here's our courses available");
			response.write(JSON.stringify(directory));
			response.end();
	}


	else if(request.url === "/addcourse" && request.method === "POST") {

		let requestBody = "";
		request.on('data', function(data){
			requestBody += data;
		})

		request.on('end', function(){
			requestBody = JSON.parse(requestBody);

			let newUser = {
				"course": requestBody.course,
			}

			directory.push(newUser);

			response.writeHead(200, {'Content-Type': 'application/JSON'});
			response.write(JSON.stringify(newUser));

			response.end();
		})
	}



	/*else if(request.url === "/updatecourse" && request.method === "PUT") {

		let requestBody = "";
		request.on('data', function(data){
			requestBody += data;
		})

		request.on('end', function(){
			requestBody = JSON.parse(requestBody);

			let newUser = {
				"course": requestBody.course,
			}

			directory.push(newUser);

			response.writeHead(200, {'Content-Type': 'application/JSON'});
			response.write(JSON.stringify(newUser));

			response.end();
		})
	}*/



	else if(request.url === "/updatecourse" && request.method === "PUT"){
			response.writeHead(200, {'Content-Type': 'text/plain'});
			response.write("Update a course to our resources");
			response.end();
	}



	else if(request.url === "/archivecourses" && request.method === "DELETE"){
			response.writeHead(200, {'Content-Type': 'application/JSON'});
			response.write("Arhive courses to our resources");
			response.end();
	}



}).listen(port);

console.log(`Server is running at port ${port}.`);